<?php

namespace common\jobs;

use common\models\Proxy;
use common\services\CheckproxyService;
use yii\base\BaseObject;

class CheckproxyJob extends BaseObject implements \yii\queue\JobInterface
{
    public $proxyID;

    public function execute($queue)
    {
        $proxyObject = Proxy::findOne(['proxyID','=',$this->proxyID]);
        $service = new CheckproxyService();
        $service->addResource($proxyObject);
        /**
         * @todo сделать 1 job на пачку курслов(курл_мульти) добавить таблицу джобов, писать результаты мультикурла
         */

    }
}