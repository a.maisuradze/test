<?php

namespace common\services;

use common\models\Proxy;

class CheckproxyService
{
    CONST TIMEOUT_IN_SECONDS=15;

    /**
     * @var \CurlMultiHandle|null
     */
    protected $multiHandle;

    /**
     * Добавляем проксю из строки ip:port в базу.
     * @param string $proxy
     * @return Proxy
     */
    public function proxy2Db(string $proxy): Proxy
    {
        $proxyArray =  explode(':', $proxy);

        $proxyObject = Proxy::findOne([
            ['address','=',$proxyArray[0]],
            ['port','=',$proxyArray[1]],
        ]);
        if(!$proxyObject) $proxyObject = new Proxy();

        $proxyObject->address = $proxyArray[0];
        $proxyObject->port = $proxyArray[1];
        $proxyObject->save();
        $proxyObject->refresh();

        return $proxyObject;
    }


    /**
     *  Проверяем общим списком доступность проксей и получаем их страну если прокси живой.
     *
     * @param array $proxyList
     * @return void
     */
    public function batchCheckAvailable(array $proxyList):void
    {
        $ch = curl_init();
        $firstString = new \stdClass();
        $firstString->fields = "city,country,countryCode,query";
        $firstString->lang = "ru";
        $firstString->query = array_shift($proxyList);
        array_unshift($proxyList, $firstString);
        $payload = json_encode($proxyList);

        curl_setopt($ch, \CURLOPT_URL, 'http://ip-api.com/batch');
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

        curl_setopt($ch, \CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, \CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, \CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, \CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, \CURLOPT_TIMEOUT, self::TIMEOUT_IN_SECONDS);

        $content = curl_exec($ch);
        /**
         * @todo получить контент, следующего вида сверить success записать страну и статус доступности прокси в базу
         * [
         * {
         * "country": "США",
         * "countryCode": "US",
         * "city": "Сан-Франциско",
         * "query": "208.80.152.201"
         * },
         * {
         * "status": "success",
         * "country": "United States",
         * "countryCode": "US",
         * "region": "VA",
         * "regionName": "Virginia",
         * "city": "Ashburn",
         * "zip": "20149",
         * "lat": 39.03,
         * "lon": -77.5,
         * "timezone": "America/New_York",
         * "isp": "Google LLC",
         * "org": "Google Public DNS",
         * "as": "AS15169 Google LLC",
         * "query": "8.8.8.8"
         * },
         *
         *
         *
         */
    }

    /**
     * Добавляем проксю на проверку
     *
     * @param $proxy
     * @return void
     */
    public function addResource(Proxy &$proxy):void
    {
        if(!$this->multiHandle){
            $this->multiHandle = curl_multi_init();
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'http://ip-api.com/php/'.$proxy->address);

        curl_setopt($ch, CURLOPT_PROXY, $proxy->address.':'.$proxy->port);
        curl_setopt($ch, \CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, \CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, \CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, \CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, \CURLOPT_TIMEOUT, self::TIMEOUT_IN_SECONDS);

        curl_multi_add_handle($this->multiHandle, $ch);
    }

    /**
     *
     * Запускаем асинхронный тест всех добавленных в $this->multiHandle курлов.
     * @return void
     */
    public function runCheckproxy():void
    {
        //execute the multi handle
        do {
            $status = curl_multi_exec($this->multiHandle, $active);
            if ($active) {
                // Wait a short time for more activity
                curl_multi_select($this->multiHandle);
            }
        } while ($active && $status == CURLM_OK);


        /**
         * @todo получить контент, записать внешний ip и другие параметры прокси в базу
         *
         * возможно надо закрывать каждый курл отдельно через
         * curl_multi_remove_handle($mh, $ch);
         * curl_close($ch);
         */
        curl_multi_close($this->multiHandle);
    }
}