<?php

use yii\db\Schema;
use yii\db\Migration;

/**
 * Class m230811_082637_create_table_proxy
 */
class m230811_082637_create_table_proxy extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('proxy',[
            'proxyID'=> Schema::TYPE_PK,
            'type' => Schema::TYPE_SMALLINT,
            'address' => Schema::TYPE_STRING,
            'port' => Schema::TYPE_STRING,
            'status' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ]);

        $this->createTable('proxy_history',[
            'proxy_historyID'=> Schema::TYPE_PK,
            'proxyID'=> Schema::TYPE_INTEGER,
            'status' => Schema::TYPE_BOOLEAN,
            'created_at' => Schema::TYPE_DATETIME,
            'updated_at' => Schema::TYPE_DATETIME,
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('proxy');
        $this->dropTable('proxy_history');
    }

}
